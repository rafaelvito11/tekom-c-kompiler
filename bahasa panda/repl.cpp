#include <iostream>
#include <string>
#include "./header/lexer.h"
#include "./header/parser.h"
#include "./header/evaluator.h"
#include "./header/environment.h"

void eval() {
  const std::string PROMPT = ">> ";
  panda::Lexer l;
  panda::Parser p;
  panda::Evaluator e;
  panda::Environment* env = new panda::Environment();
  while(true) {ƒ
    std::string line;
    std::cout << PROMPT;
    std::getline(std::cin, line);
    l.New(line);
    p.New(l);
    panda::Program* program = p.ParseProgram();
    if (p.Errors().size()) {
      for (auto error : p.Errors()) {
        std::cout << error << std::endl;
      }
      continue;
    }
    panda::Object* o = e.Eval(program, env);
    std::cout << "type: " << o->Type() << std::endl;
    std::cout << o->Inspect() << std::endl;
  }
}

int main() {
  eval();
  return 0;
}