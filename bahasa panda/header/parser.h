#ifndef PANDA_PARSER_H_
#define PANDA_PARSER_H_

#include <vector>
#include <string>
#include <unordered_map>
#include "lexer.h"
#include "ast.h"

namespace panda {

class Parser;

typedef Expression* (Parser::*PrefixParseFn)(); 
typedef Expression* (Parser::*InfixParseFn)(Expression* );  


enum Precedence{
  LOWEST,
  EQUALS,      
  LESSGREATER, 
  SUM,         
  PRODUCT,     
  PREFIX,      
  CALL,        
  INDEX,       
};

extern std::unordered_map<TokenType, Precedence> precedences;

class Parser {
 public:
  void New(Lexer& l);
  std::vector<std::string> Errors() { return errors; }
  Program* ParseProgram();
 private:
  void nextToken();
  bool expectPeek(const TokenType& t);
  Precedence peekPrecedence();
  Precedence curPrecedence();

  Statement* parseStatement();
  LetStatement* parseLetStatement();
  RefStatement* parseRefStatement();
  ReturnStatement* parseReturnStatement();
  ExpressionStatement* parseExpressionStatement();
  BlockStatement* parseBlockStatement();

  Expression* parseExpression(Precedence precedence);
  
  Expression* parseIdentifier();  
  Expression* parseIntegerLiteral();
  Expression* parseStringLiteral();
  Expression* parsePrefixExpression();
  Expression* parseBoolean();
  std::vector<Identifier*> parseFunctionParameters();
  Expression* parseFunctionLiteral();
  Expression* parseArrayLiteral();
  Expression* parseGroupedExpression();
  Expression* parseIfExpression();
  Expression* parseWhileExpression();

  std::vector<Expression*> parseExpressionList();
  Expression* parseCallExpression(Expression* function);
  Expression* parseIndexExpression(Expression* array);
  Expression* parseInfixExpression(Expression* left);

  void noPrefixParseFnError(TokenType type);

  void peekError(const TokenType& t);
  
  Lexer l;

  Token curToken;
  Token peekToken;
  
  std::unordered_map<TokenType, PrefixParseFn> prefixParseFns;
  std::unordered_map<TokenType, InfixParseFn> infixParseFns;

  std::vector<std::string> errors;
};

}


#endif  